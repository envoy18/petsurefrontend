import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '../../node_modules/@angular/compiler/src/core';
import { AddClaimsComponent } from './add-claims/add-claims.component';
import { HomeComponent } from './home/home.component';

const router: Routes = [
  { path: '', component: HomeComponent },
  { path: 'addclaims', component: AddClaimsComponent }
];


export const routes: ModuleWithProviders  = RouterModule.forRoot(router);
export const appRoutes = RouterModule.forChild(router);
