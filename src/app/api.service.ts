import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private  httpClient:  HttpClient) { }
  API_URL  =  'http://localhost:51081';

  
  getPetList(ownerId){
    var owner = {"id": ownerId};
    return  this.httpClient.post(`${this.API_URL}/LoadPetList` , owner);
  }
  
}
