import { Component, OnInit } from '@angular/core';
import { ApiService } from  '../api.service';

@Component({
  selector: 'app-add-claims',
  templateUrl: './add-claims.component.html',
  styleUrls: ['./add-claims.component.css']
})
export class AddClaimsComponent implements OnInit {

  constructor(private apiService : ApiService) { }

  private  pets:  Array<object> = [];
  private selectedPetId;
  private selectedPetName;

  ngOnInit() {
    this.getPets();
  }

  public  getPets(){
    this.apiService.getPetList(1).subscribe((data:  Array<object>) => {
        this.pets  =  data;
        this.selectedPetId = data[0]["id"];
        this.selectedPetName = data[0]["PetName"];
        console.log(this.selectedPetName);
    });
  }

  public getSelectedPet(args){
    this.selectedPetId = args.target.value;
    this.selectedPetName = args.target.options[args.target.selectedIndex].text;
  }

}
